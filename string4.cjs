function titlecase(obj,expected_obj,change_string){
   let str="";
   if(typeof(obj)=="object" && typeof(expected_obj)=="object"){
    for(let idx in expected_obj){
        if(obj[idx]==undefined){
            str=str+change_string(expected_obj[idx])+" ";
        }
        else{
            str=str+change_string(obj[idx]+" ")
        }
    }
    return str;
   }
   else{
    return [];
   }
}
module.exports=titlecase;