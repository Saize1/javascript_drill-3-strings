const titlecase=require("./string4.cjs");

let obj={
    "first_name": "JoHN", 
};
let expected_obj={
    "first_name": "JoHN",
     "middle_name": "doe",
      "last_name": "SMith"

};
function change_string(str){
    first_char=str.charAt(0).toUpperCase()
    remaining_char=str.substring(1).toLowerCase();
    return first_char +remaining_char; 
}
const ans= titlecase(obj,expected_obj,change_string);
console.log(ans);